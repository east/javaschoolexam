package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;
import java.util.Objects;


public class Calculator {

	private enum Operations {
		LEFT_BRACKET("(", -1),
		RIGHT_BRACKET(")", -1),
		SUM("+", 0),
		SUB("-", 0),
		MUL("*", 1),
		DIV("/", 1);
		
		private String op;
		private int priority;
		
		Operations(String op, int priority) {
			this.op = op;
			this.priority = priority;
		}
		
		public String getOp() {
			return this.op;
		}
		
		public int getPriority() {
			return this.priority;
		}
		
		public static Operations value(String sOp) {
			for(Operations op: Operations.values()) {
				if(Objects.equals(op.getOp(), sOp)) {
					return op;
				}
			}
			return null;
		}
	}
   
    LinkedList<Double> operands = new LinkedList<Double>();
    LinkedList<Operations> operations = new LinkedList<Operations>();
    
	/**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
    	
    	if(statement == null || statement.isEmpty()) return null;

        for(int i = 0; i < statement.length(); i++) {
        	char c = statement.charAt(i);
        	
        	if(Character.isDigit(c)) {
        		// try to parse double val
        		int parsedLen = parseDouble(statement.substring(i));
        		if(parsedLen == 0) return null;
        		
        		i += parsedLen-1;
        		        		
    			// Perform operation if it's division or multiplication
        		Operations op = operations.peek();
        		if(op != null && (op == Operations.MUL || op == Operations.DIV)) {
        			if(!performOp()) return null;
        		}
        	}
        	else if(Character.isWhitespace(c)) {
        		continue;
        	}
        	else { // should be arithmetic sign or bracket
        		Operations op = Operations.value(Character.toString(c));
        		if(op == null || !handleOp(op)) return null;
        	}
        }
        
        while(!operations.isEmpty()) {
        	if(!performOp()) return null;
        }
        
        Double result = operands.pop();
        
        if(result == Math.floor(result)) {
        	return String.valueOf(result.intValue());
        }
        return result.toString();
    }

    /**
     * Parse statement for double value
     * 
     * @param s statement that contains double value
     * @return length of substring with double value
     */
    private int parseDouble(String s) {
		// try to parse double val
		String val = new String();
		
		for(int i = 0; i < s.length(); i++) {
			final char asciiDot = 46;  			
			char c = s.charAt(i);
			if(!Character.isDigit(c)) {
				if(c != asciiDot) {
					break;
				}
				
    			String dot = Character.toString(c);
				if(val.contains(dot)) {
					// Already has dot (wrong format)
					return 0;
				}
			}
			val += Character.toString(c);
		}
		
		operands.push(Double.valueOf(val));
		
		return val.length();
    }

    /**
     * Check operation and perform appropriate actions
     * 
     * @param op operation
     * @return true on success, otherwise returns false
     */
    private boolean handleOp(Operations op)
    {
    	switch(op) {
    	case SUB:
    	case SUM:
    	case MUL:
    	case DIV: {
    		
    		if(operands.isEmpty()) return false;
    		
    		while(!operations.isEmpty() && operations.peek().getPriority() >= op.getPriority()) {
    			if(!performOp()) return false;
    		}
    		
    		operations.push(op);
    		break;
    	}
    	case RIGHT_BRACKET: {
    		// calculate expression in brackets
    		while(!operations.isEmpty() && operations.peek() != Operations.LEFT_BRACKET) {
    			if(!performOp()) return false;
    		}
    		
    		if(operations.isEmpty()) {
    			// there is no left bracket in the expression
    			return false;
    		}
    		// remove left bracket from ops stack
    		operations.pop();
    		break;
    	}
    	default: {
    		operations.push(op);
    		break;
    		}
    	}
    
    	return true;
    }
    
    /**
     * Pops operation from top of the operations stack, perform it and push result onto operands stack
     * 
     * @return true on success, otherwise returns false
     */
    private boolean performOp()
    {
    	if(operands.size() < 2) return false;
    	
    	Operations op = operations.pop();
    	
    	Double secondOperand = operands.pop();
    	Double firstOperand = operands.pop();
    	
    	Double result;
    	
    	switch(op) {
	    	case MUL: {
	    		result = firstOperand * secondOperand;
	    		break;
	    	}
	    	case DIV: {
	    		result = firstOperand / secondOperand;
	    		break;
	    	}
	    	case SUM: {
	    		result = firstOperand + secondOperand;
	    		break;
	    	}
	    	case SUB: {
	    		result = firstOperand - secondOperand;
	    		break;
	    	}
	    	default: return false;
    	}
    	
        if(result.isInfinite() || result.isNaN()) {
        	return false;
        }
    	
    	operands.push(result);
    	return true;
    }
}

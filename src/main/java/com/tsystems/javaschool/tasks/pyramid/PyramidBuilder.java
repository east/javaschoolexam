package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.Collections;
import java.util.ListIterator;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
    	
    	if(inputNumbers.contains(null)) {
    		throw new CannotBuildPyramidException();
    	}
    	
        int pyramidSize = 1;
        long pyramidElements = 1;
        for(; inputNumbers.size() > pyramidElements; pyramidSize++, pyramidElements += pyramidSize);

        if(inputNumbers.size() != pyramidElements) {
          throw new CannotBuildPyramidException();
        }

        Collections.sort(inputNumbers);

        final int rowSize = pyramidSize * 2 - 1;
        int[][] pyramid = new int[pyramidSize][rowSize];

        ListIterator<Integer> numbersIter = inputNumbers.listIterator();

        for(int row = 0, elementsPut = 0; row < pyramidSize; row++) {

            int pyramidRowCount = (row + 1) * (row + 2) / 2;

            for(int col = rowSize / 2 - row; elementsPut < pyramidRowCount; col += 2, elementsPut++) {
              pyramid[row][col] = numbersIter.next();
            }
        }

        return pyramid;
    }


}
